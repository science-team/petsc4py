Alternative PETSc builds for petsc4py
=====================================

petsc4py is available for python3 with support for 
real numbers (python3-petsc4py-real)
and complex numbers (python3-petsc4py-complex).

Either build is accessible at runtime.  By default petsc4py uses the
default PETSc build set in /usr/lib/petsc (which can be selected with
"update-alternatives --config petsc")

Other PETSc builds can be selected at runtime setting (or exporting)
the PETSC_DIR environment variable.

Examples include:

PETSC_DIR=/usr/lib/petscdir/petsc-real
 selects the preferred real-number version of PETSc (by default the
 latest version, but can be selected with
 "update-alternatives --config libpetsc_real.so"

PETSC_DIR=/usr/lib/petscdir/petsc-complex
 selects the preferred complex-number version of PETSc (by default the
 latest version, but can be selected with
 "update-alternatives --config libpetsc_complex.so"


Specific PETSc versions and builds as found under /usr/lib/petscdir
can also be selected with PETSC_DIR.


For example,
  PETSC_DIR=/usr/lib/petscdir/petsc-complex python3 -c "import petsc4py; print(petsc4py.__path__)"
prints the path to the petsc4py module built against the preferred
complex-number build of PETSc.

Alternative builds include 64-bit builds of PETSc (64-bit indexing)
e.g.
  PETSC_DIR=/usr/lib/petscdir/petsc64-real
  PETSC_DIR=/usr/lib/petscdir/petsc64-complex
